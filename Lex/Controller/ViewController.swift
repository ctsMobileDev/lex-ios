//
//  ViewController.swift
//  Lex
//
//  Created by ChawTech Solutions on 07/03/22.
//

import UIKit
import SkyFloatingLabelTextField
import Toast_Swift
import LanguageManager_iOS
//import MOLH

class ViewController: UIViewController {

    @IBOutlet weak var emailTf: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTf: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameTf: SkyFloatingLabelTextField!
    @IBOutlet weak var dobTxtField: SkyFloatingLabelTextField!
    @IBOutlet weak var quesTableView: UITableView!
    @IBOutlet weak var langBtn: UIButton!
    
    @IBOutlet weak var womanBtn: UIButton!
    @IBOutlet weak var manBtn: UIButton!
    @IBOutlet weak var smokeBtn: UIButton!
    @IBOutlet weak var nonSmokeBtn: UIButton!
    var lang = "en"
    var questionsList = [QuestionsList_Struct]()
    var duplicateQuestionsList = [QuestionsList_Struct]()
    var testsList = [testList_Struct]()
    var isMale = false
    var isSmoke = false
    let datePicker = UIDatePicker()
    var year = ""
    var month = ""
    var structToJson = [Any]()
    var needToResetSelection = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showDatePicker()
        view.backgroundColor = UIColor.white
//        if UserDefaults.standard.value(forKey: "lang") != nil {
//            self.lang = UserDefaults.standard.value(forKey: "lang") as! String
//            self.langBtn.setTitle(self.lang == "he" ? "Hebrew".localized() : "English".localized(), for: .normal)
//        }
        self.needToResetSelection = false
        if L102Language.currentAppleLanguage() == "he" {
            L102Language.setAppleLAnguageTo(lang: "he")
            self.langBtn.setTitle("Hebrew".localized(), for: .normal)
            self.lang = "he"
            self.firstNameTf.isLTRLanguage = false
            self.dobTxtField.isLTRLanguage = false
            self.lastNameTf.isLTRLanguage = false
            self.emailTf.isLTRLanguage = false
            
        } else {
            L102Language.setAppleLAnguageTo(lang: "en")
            self.langBtn.setTitle("English".localized(), for: .normal)
            self.lang = "en"
            self.firstNameTf.isLTRLanguage = true
            self.dobTxtField.isLTRLanguage = true
            self.lastNameTf.isLTRLanguage = true
            self.emailTf.isLTRLanguage = true
        }
        self.getQuestionsList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.questionsList = self.duplicateQuestionsList
        DispatchQueue.main.async {
            self.manBtn.setImage(UIImage.init(named: "man"), for: .normal)
            self.womanBtn.setImage(UIImage.init(named: "woman"), for: .normal)
            self.smokeBtn.setImage(UIImage.init(named: "smoke"), for: .normal)
            self.nonSmokeBtn.setImage(UIImage.init(named: "noSmoke"), for: .normal)
            self.dobTxtField.text = ""
            self.firstNameTf.text = ""
            self.lastNameTf.text = ""
            self.emailTf.text = ""
            self.quesTableView.reloadData()
        }
        
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
//        datePicker.accessibilityLanguage = L102Language.currentAppleLanguage()
        let loc = Locale(identifier: L102Language.currentAppleLanguage())
        datePicker.locale = loc
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dobTxtField.inputAccessoryView = toolbar
        dobTxtField.inputView = datePicker
        
    }

      @objc func donedatePicker(){

          let formatter = DateFormatter()
          formatter.dateFormat = "dd/MM/yyyy"
          dobTxtField.text = formatter.string(from: datePicker.date)
//          formatter.locale = Locale
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy"
          self.year = dateFormatter.string(from: self.datePicker.date)
          dateFormatter.dateFormat = "MM"
          self.month = dateFormatter.string(from: self.datePicker.date)
          self.view.endEditing(true)
     }

     @objc func cancelDatePicker(){
        self.view.endEditing(true)
      }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        self.openActionSheet()
    }
    
    @IBAction func viewTestsAction(_ sender: UIButton) {
        if self.dobTxtField.text!.isEmpty {
            self.view.makeToast("Please select your DOB".localized())
            return
        } else if checkIfAnyQuestionIsNotAnswered() {
            self.view.makeToast("Please answer all the question.".localized())
            return
        }
//        do {
//            let jsonData = try JSONEncoder().encode(self.questionsList)
//            if let structToJsonn = String(data: jsonData, encoding: .utf8)
//            {
//                print(structToJsonn)
//                self.structToJson = structToJsonn
//            }
//
//
//        } catch {  }
        
        do {
            let jsonData = try JSONEncoder().encode(self.questionsList)
            
            let thanedaarArr = try JSONSerialization.jsonObject(with: jsonData, options:[])
//            print(thanedaarArr)
            self.structToJson = thanedaarArr as! [Any]
        } catch {  }
        
        self.postWorkStatusToAdminAPI()
        
    }
    
    func checkIfAnyQuestionIsNotAnswered() -> Bool {
        var notAnswered = false
        for i in self.questionsList {
            if i.answer == "" {
                notAnswered = true
                break
            }
        }
        return notAnswered
    }
    
    @IBAction func sexAction(_ sender: UIButton) {
        isMale = sender.tag == 101 ? true : false
        if isMale {
            self.manBtn.setImage(UIImage.init(named: "men-check"), for: .normal)
            self.womanBtn.setImage(UIImage.init(named: "woman"), for: .normal)
        } else {
            self.womanBtn.setImage(UIImage.init(named: "lady-check"), for: .normal)
            self.manBtn.setImage(UIImage.init(named: "man"), for: .normal)
        }

    }
    
    @IBAction func smokerAction(_ sender: UIButton) {
        isSmoke = sender.tag == 201 ? true : false
        if !isSmoke {
            self.smokeBtn.setImage(UIImage.init(named: "smoke-check"), for: .normal)
            self.nonSmokeBtn.setImage(UIImage.init(named: "noSmoke"), for: .normal)
        } else {
            self.nonSmokeBtn.setImage(UIImage.init(named: "noSmoke-check"), for: .normal)
            self.smokeBtn.setImage(UIImage.init(named: "smoke"), for: .normal)
        }
    }
    
    @IBAction func yesNoCellAction(_ sender: UIButton) {
        var superview = sender.superview
        while let view = superview, !(view is UITableViewCell) {
            superview = view.superview
        }
        guard let cell = superview as? UITableViewCell else {
            print("button is not contained in a table view cell")
            return
        }
        guard let indexPath = quesTableView.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        // We've got the index path for the cell that contains the button, now do something with it.
        print("button is in row \(indexPath.row)")
        self.questionsList[indexPath.row].answer = (sender.titleLabel?.text!)!
    }
    
    
    func openActionSheet() {
        let alert = UIAlertController(title: "Language".localized(), message: "Please Select language".localized(), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "English".localized(), style: .default , handler:{ (UIAlertAction)in
            L102Language.setAppleLAnguageTo(lang: "en")
            UserDefaults.standard.set("en", forKey: "lang")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextView.appearance().semanticContentAttribute = .forceLeftToRight
            self.firstNameTf.textAlignment = .right
//            self.firstNameTf.semanticContentAttribute = .forceLeftToRight
//            self.dobTxtField.semanticContentAttribute = .forceLeftToRight
//            self.dobTxtField.layoutIfNeeded()
            Bundle.setLanguage("en")
//            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//            MOLH.reset()
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            LanguageManager.shared.setLanguage(language: .en)
//                { title -> UIViewController in
//                  let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                  // the view controller that you want to show after changing the language
//                  return storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                } animation: { view in
//                  // do custom animation
//                  view.transform = CGAffineTransform(scaleX: 2, y: 2)
//                  view.alpha = 0
//                }
        }))
        
        alert.addAction(UIAlertAction(title: "Hebrew".localized(), style: .default , handler:{ (UIAlertAction)in
            L102Language.setAppleLAnguageTo(lang: "he")
            UserDefaults.standard.set("he", forKey: "lang")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextView.appearance().semanticContentAttribute = .forceRightToLeft
            self.firstNameTf.textAlignment = .left
//            self.firstNameTf.semanticContentAttribute = .forceRightToLeft
//            self.dobTxtField.semanticContentAttribute = .forceRightToLeft
//            self.dobTxtField.layoutIfNeeded()
            Bundle.setLanguage("he")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            LanguageManager.shared.setLanguage(language: .he)
//                { title -> UIViewController in
//                  let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                  // the view controller that you want to show after changing the language
//                  return storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                } animation: { view in
//                  // do custom animation
//                  view.transform = CGAffineTransform(scaleX: 2, y: 2)
//                  view.alpha = 0
//                }
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    //MARK: : GE Questions API
    func getQuestionsList() {
        if Reachability.isConnectedToNetwork() {
            showProgressOnView(appDelegateInstance.window!)
            let param:[String:String] = [:]
            let url = PROJECT_URL.getQuestionListApi + "?lang=\(self.lang)"
            let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            ServerClass.sharedInstance.getRequestWithUrlParameters(param, path: urlString, successBlock: { (json) in
                debugPrint(json)
                hideAllProgressOnView(appDelegateInstance.window!)
                let success = json["success"].boolValue
                if success {
                    let data = json["data"].arrayValue
                    self.questionsList.removeAll()
                    for i in 0..<data.count {
                        
                        let question = data[i]["question"].stringValue
                        let question_type = data[i]["question_type"].stringValue
                        let order = data[i]["order"].intValue
                        let id = data[i]["id"].stringValue
                        
                        self.questionsList.append(QuestionsList_Struct.init(order: order, id: id, question: question, question_type: question_type))
                        self.duplicateQuestionsList.append(QuestionsList_Struct.init(order: order, id: id, question: question, question_type: question_type))
                    }
                    
                    DispatchQueue.main.async {
                        self.quesTableView.reloadData()
                    }
                }
                else {
                    UIAlertController.showInfoAlertWithTitle("Message", message: json["message"].stringValue, buttonTitle: "Okay")
                }
            }, errorBlock: { (NSError) in
                UIAlertController.showInfoAlertWithTitle("Alert", message: kUnexpectedErrorAlertString, buttonTitle: "Okay")
                hideAllProgressOnView(appDelegateInstance.window!)
            })
            
        }else{
            hideAllProgressOnView(appDelegateInstance.window!)
            UIAlertController.showInfoAlertWithTitle("Alert", message: "Please Check internet connection", buttonTitle: "Okay")
        }
    }
    
    func postWorkStatusToAdminAPI() {
        if Reachability.isConnectedToNetwork() {
//            showProgressOnView(appDelegateInstance.window!)
            if let objFcmKey = UserDefaults.standard.object(forKey: "fcm_key") as? String
            {
                fcmKey = objFcmKey
            }
            else
            {
                //                fcmKey = ""
                fcmKey = "abcdef"
            }
                
            let param:[String:Any] = ["lang": self.lang, "age_year": self.year,"age_month": self.month, "data": structToJson]
            print(param)
            ServerClass.sharedInstance.postRequestWithUrlParameters(param, path: BASE_URL + PROJECT_URL.submitAnswerApi, successBlock: { (json) in
                print(json)
//                hideAllProgressOnView(appDelegateInstance.window!)
                let success = json["success"].boolValue
                if success {
                    let data = json["data"].arrayValue
                    self.testsList.removeAll()
                    for i in 0..<data.count {
                        
                        let description = data[i]["description"].stringValue
                        let answer = data[i]["answer"].stringValue
                        let order = data[i]["order"].intValue
                        let id = data[i]["id"].stringValue
                        
                        self.testsList.append(testList_Struct.init(order: order, id: id, description: description, answer: answer))
                    }
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TestsVC") as! TestsVC
//                    self.navigationController?.pushViewController(vc, animated: true)
                    vc.modalPresentationStyle = .fullScreen
                    vc.testsList = self.testsList
                    self.needToResetSelection = true
                    self.present(vc, animated: true)
                }
                else {
                    self.view.makeToast("\(json["message"].stringValue)")
                }
            }, errorBlock: { (NSError) in
                UIAlertController.showInfoAlertWithTitle("Alert", message: kUnexpectedErrorAlertString, buttonTitle: "Okay")
//                hideAllProgressOnView(appDelegateInstance.window!)
            })
        }else{
//            hideAllProgressOnView(appDelegateInstance.window!)
//            //UIAlertController.showInfoAlertWithTitle("Alert", message: "Please Check internet connection", buttonTitle: "Okay")
        }
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questionsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
        let info = self.questionsList[indexPath.row]
        cell.quesLbl.text = info.question
        
        if info.answer == "Yes" || info.answer == "כן" {
            cell.noLbl.isSelected = false
            cell.yesLbl.isSelected = true
        } else if info.answer == "No" || info.answer == "לא" {
            cell.noLbl.isSelected = true
            cell.yesLbl.isSelected = false
        } else {
            cell.noLbl.isSelected = false
            cell.yesLbl.isSelected = false
        }
        
        cell.selectionStyle = .none
        if needToResetSelection {
            cell.yesLbl.isSelected = false
            cell.noLbl.isSelected = false
        }
        
        return  cell
    }
    
    
}

//extension UITextField {
//    open override func awakeFromNib() {
//        super.awakeFromNib()
//        if L102Language.currentAppleLanguage() == "he" {
//            if textAlignment == .natural {
//                self.textAlignment = .right
//            }
//        }
//    }
//}
