//
//  TestsVC.swift
//  Lex
//
//  Created by ChawTech Solutions on 08/03/22.
//

import UIKit

class TestsVC: UIViewController {
    
    var testsList = [testList_Struct]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
}

extension TestsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.testsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        let info = self.testsList[indexPath.row]
        cell.cellSubLbl.text = info.description
        cell.cellMainLbl.text = info.answer
        cell.selectionStyle = .none
        return cell
    }
    
    
}
