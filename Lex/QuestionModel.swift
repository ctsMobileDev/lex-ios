//
//  QuestionModel.swift
//  Lex
//
//  Created by Ritesh Sinha on 15/03/22.
//

import Foundation

struct QuestionsList_Struct: Codable {
    var order: Int = 0
    var id: String = ""
    var question: String = ""
    var question_type: String = ""
    var answer: String = ""
    
}

struct testList_Struct: Codable {
    var order: Int = 0
    var id: String = ""
    var description: String = ""
    var answer: String = ""
    
}
