//
//  ReportCell.swift
//  Lex
//
//  Created by Ritesh Sinha on 25/03/22.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var cellSubLbl: UILabel!
    @IBOutlet weak var cellMainLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
